package com.dh.test;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import org.apache.log4j.BasicConfigurator;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


/**
 * Hello world!
 *
 */
public class ReadDB 
{
	static Logger logger=Logger.getLogger(ReadDB.class);

    public static void main( String[] args ) throws Exception
    {
    
    	PropertyConfigurator.configure("log4j.properties");
    	logger.info("main() is executed"); 
    	
    	Class.forName("org.mariadb.jdbc.Driver");
    	logger.debug("forname() is executed");
    	
		Connection con=DriverManager.getConnection("jdbc:mariadb://localhost:3306/dhanush", "root", "root");
    	logger.error("connection is executed");

		Statement st=con.createStatement();
    	logger.trace("statement is executed");
    	
    	String query="create table IF NOT EXISTS read_table1(EMPID varchar(40),EMPNAME varchar(40),DESIGNATION varchar(40))";
    	logger.warn("query is executed");

    	ResultSet rs=st.executeQuery(query);
    	logger.fatal("resultset is executed");

    			System.out.println("table created");
    	    	

    	
    	
    	String sql = " INSERT INTO dhanush.read_table1 VALUES(?,?,?) ";
    	logger.debug("insert query is executed");


    		
    	        BufferedReader bReader = new BufferedReader(new FileReader("D:\\data11.csv"));
    	    	logger.warn("bufferedreader is executed");

    	        String line = ""; 
    	        int num=1;
    	        
    	        while ((line = bReader.readLine()) != null) 
    	        {
    	        	logger.fatal("read the data from csv file");


    	                if (line != null) 
    	                {
    	                    String[] array = line.split(",+"); //comma seperated values
    	                    for(String result:array)
    	                    {
    	                        System.out.println(result);
    	                    }
    	                    
    	 //Create preparedStatement here and set them and execute them
    	                    if(num!=1)
    	                    {
    	                PreparedStatement ps = con.prepareStatement(sql);
    	               
    	                 ps.setString(1,array[0]);
    	                 ps.setString(2,array[1]);
    	                 ps.setString(3,array[2]);
    	             
    	                 ps.executeUpdate();
    	                 ps. close();
    	   //Assuming that your line from file after split will follow that sequence
    	                    }
    	                    else
    	                    {
    	                    	   num++;
    	                    }
    	                    
    	             
    	                } 
    	               
    	            
    	      
    			}
    	        bReader.close();
    	        
    			}
}


    