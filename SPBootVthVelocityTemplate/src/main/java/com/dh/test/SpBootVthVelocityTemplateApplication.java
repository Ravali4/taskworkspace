package com.dh.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpBootVthVelocityTemplateApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpBootVthVelocityTemplateApplication.class, args);
	}

}

